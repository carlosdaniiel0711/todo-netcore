using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

using ToDoNetCore.Models;
using ToDoNetCore.Dtos.User;
using ToDoNetCore.Services;
using System.IdentityModel.Tokens.Jwt;
using Microsoft.IdentityModel.Tokens;
using System.Security.Claims;
using System.Collections.Generic;
using System.Text;
using System;
using Microsoft.Extensions.Configuration;

namespace ToDoNetCore.Data
{
    public class AuthRepository : IAuthRepository
    {
        private readonly ToDoContext _context;
        private readonly IConfiguration _config;

        public AuthRepository(ToDoContext context, IConfiguration config)
        {
            _context = context;
            _config = config;
        }

        public async Task<ServiceResponse<string>> Login(string username, string password)
        {
            var response = new ServiceResponse<string>();
            var user = await _context.Users
                .Include(user => user.Tasks)
            .FirstOrDefaultAsync(user => user.Username.Equals(username));

            if (user != null)
            {
                if (VerifyPassword(password, user.PasswordHash, user.PasswordSalt))
                {
                    response.Data = CreateToken(user);
                }
                else
                {
                    response.Success = false;
                    response.Message = "Senha incorreta";
                }
            }
            else
            {
                response.Success = false;
                response.Message = $"Usuário {username} não encontrado";
            }


            return response;
        }

        public async Task<ServiceResponse<GetUserDTO>> Register(string username, string password)
        {
            if (!(await Exists(username)))
            {
                User user = null;
                CreatePasswordHash(password, out byte[] hash, out byte[] salt);

                user = new User()
                {
                    Username = username,
                    PasswordHash = hash,
                    PasswordSalt = salt
                };

                await _context.Users.AddAsync(user);
                await _context.SaveChangesAsync();

                return new ServiceResponse<GetUserDTO>() { Data = new GetUserDTO { Id = user.Id, Username = user.Username } };
            }
            else
            {
                return new ServiceResponse<GetUserDTO>() { Data = null, Success = false, Message = $"Usuário {username} já existe" };
            }
        }

        private async Task<bool> Exists(string username)
        {
            return await _context.Users.AnyAsync(user => user.Username.Equals(username));
        }

        private void CreatePasswordHash(string password, out byte[] hash, out byte[] salt)
        {
            using (var hmac = new System.Security.Cryptography.HMACSHA512())
            {
                salt = hmac.Key;
                hash = hmac.ComputeHash(System.Text.Encoding.UTF8.GetBytes(password));
            }
        }

        private bool VerifyPassword(string plainTextPassword, byte[] hash, byte[] salt)
        {
            using (var hmac = new System.Security.Cryptography.HMACSHA512(salt))
            {
                var newHash = hmac.ComputeHash(System.Text.Encoding.UTF8.GetBytes(plainTextPassword));

                for (var i = 0; i < newHash.Length; i++)
                {
                    if (newHash[i] != hash[i])
                        return false;
                }
            }

            return true;
        }

        private string CreateToken(User user)
        {
            string jwtSecret = _config.GetSection("AppSettings:JwtSecret").Value;
            SymmetricSecurityKey secret = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(jwtSecret));
            SigningCredentials credentials = new SigningCredentials(secret, SecurityAlgorithms.HmacSha512Signature);


            List<Claim> claims = new List<Claim>()
    {
        new Claim(ClaimTypes.NameIdentifier, user.Id.ToString()),
        new Claim(ClaimTypes.Name, user.Username)
    };


            SecurityTokenDescriptor tokenDescription = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(claims),
                SigningCredentials = credentials,
                Expires = DateTime.Now.AddDays(1)
            };


            JwtSecurityTokenHandler tokenHandler = new JwtSecurityTokenHandler();
            SecurityToken token = tokenHandler.CreateToken(tokenDescription);


            return $"Bearer {tokenHandler.WriteToken(token)}";
        }
    }
}