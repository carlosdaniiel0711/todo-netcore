using ToDoNetCore.Models;

namespace ToDoNetCore.Data
{
    using Microsoft.EntityFrameworkCore;

    public class ToDoContext : DbContext
    {
        public ToDoContext (DbContextOptions<ToDoContext> options) : base(options)
        {
            
        }

        public DbSet<TodoItem> ToDoItems { get; set; }
        public DbSet<User> Users { get; set; }
    }
}