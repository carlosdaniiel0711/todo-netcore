using System.Threading.Tasks;
using ToDoNetCore.Models;
using ToDoNetCore.Dtos.User;
using ToDoNetCore.Services;

namespace ToDoNetCore.Data
{
    public interface IAuthRepository
    {
        Task<ServiceResponse<GetUserDTO>> Register(string username, string password);
        Task<ServiceResponse<string>> Login(string username, string password);
    }
}