using System;

namespace ToDoNetCore.Models
{
    public class ObjectNotFoundException : SystemException
    {
        public ObjectNotFoundException(string message) : base(message)
        {
            
        }
    }
}