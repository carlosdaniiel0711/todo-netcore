using System;

namespace ToDoNetCore.Models
{
    public class TodoItem
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public bool IsComplete { get; set; }
        public DateTime CreatedAt { get; set; } = DateTime.Now;
        
        public User User { get; set; }
        public long UserId { get; set; }
    }
}