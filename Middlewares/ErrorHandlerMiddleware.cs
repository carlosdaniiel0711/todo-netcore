using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;

using ToDoNetCore.Models;

namespace ToDoNetCore.Middlewares
{
    public class ErrorHandlerMiddleware: IMiddleware
    {
        public async Task InvokeAsync(HttpContext context, RequestDelegate next)
        {
            try
            {
                await next(context);
            }
            catch (Exception exception)
            {
                await HandleExceptionAsync(context, exception);
            }
        }
        
        private static async Task HandleExceptionAsync(HttpContext context, Exception exception)
        {
            var exceptionObject = new ExceptionResponse(exception.Message, context.Request.Path.Value);
            var exceptionSerialized = JsonConvert.SerializeObject(exceptionObject);

            if (!context.Response.HasStarted)
            {
                context.Response.ContentType = "application/json";
                context.Response.StatusCode = exception is ObjectNotFoundException ? 404 : 500;

                await context.Response.WriteAsync(exceptionSerialized);
            }
        }
    }
}