using System;

namespace ToDoNetCore.Middlewares
{
    public class ExceptionResponse
    {
        public string Message { get; set; }
        public DateTime Date { get; set; }

        public string Path { get; set; }

        public ExceptionResponse(string message, string path)
        {
            Message = message;
            Path = path;
            Date = DateTime.Now;
        }
    }
}