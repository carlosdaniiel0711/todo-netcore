using System.Collections.Generic;
using System.Threading.Tasks;

using ToDoNetCore.Models;

namespace ToDoNetCore.Services.ToDoService
{
    public interface IToDoService
    {
        Task<ServiceResponse<IEnumerable<TodoItem>>> GetAll();
        Task<ServiceResponse<TodoItem>> GetById(long id);
        Task<TodoItem> Insert(TodoItem item);
        Task<IEnumerable<TodoItem>> Insert(IEnumerable<TodoItem> items);
        Task<TodoItem> Update(TodoItem item);
        Task<TodoItem> Delete(long id);
    }
}