using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Http;

using ToDoNetCore.Models;
using ToDoNetCore.Data;

namespace ToDoNetCore.Services.ToDoService  
{
    public class ToDoService : IToDoService
    {
        private readonly ToDoContext _context;
        private readonly IHttpContextAccessor _httpContext;

        public ToDoService(ToDoContext context, IHttpContextAccessor httpContext)
        {
            _context = context;
            _httpContext = httpContext;
        }

        public async Task<ServiceResponse<IEnumerable<TodoItem>>> GetAll()
        {
            IEnumerable<TodoItem> data = await _context.ToDoItems.ToListAsync();
            
            return new ServiceResponse<IEnumerable<TodoItem>>
            {
                Data = data
            };
        }

        public async Task<ServiceResponse<TodoItem>> GetById(long id)
        {
            var response = new ServiceResponse<TodoItem>();
            var item = await _context.ToDoItems.FindAsync(id);

            if (item != null)
            {
                response.Data = item;
            }
            else
            {
                throw new ObjectNotFoundException("Registro não encontrado na base de dados");
            }

            return response;
        }

        public async Task<TodoItem> Insert(TodoItem item)
        {
            item.UserId = GetCurrentUserId();

            await _context.ToDoItems.AddAsync(item);
            await _context.SaveChangesAsync();

            return item;
        }

        public async Task<IEnumerable<TodoItem>> Insert(IEnumerable<TodoItem> items)
        {
            var itemsAdicionados = new List<TodoItem>();

            foreach(var item in items)
                itemsAdicionados.Add(await Insert(item));

            return itemsAdicionados;
        }

        public async Task<TodoItem> Update(TodoItem item)
        {
            if (await Exists(item.Id))
            {
                _context.ToDoItems.Update(item);
                await _context.SaveChangesAsync();
            }

            return item;
        }

        public async Task<TodoItem> Delete(long id)
        {
            var item = (await GetById(id)).Data;

            if (item != null)
            {
                _context.ToDoItems.Remove(item);
                await _context.SaveChangesAsync();
            }

            return item;
        }

        private async Task<bool> Exists(long id)
        {
            return await _context.ToDoItems.AnyAsync(item => item.Id.Equals(id));
        }

        private long GetCurrentUserId() 
        {
            return int.Parse(_httpContext.HttpContext.User.FindFirst(System.Security.Claims.ClaimTypes.NameIdentifier).Value);
        }
    }
}