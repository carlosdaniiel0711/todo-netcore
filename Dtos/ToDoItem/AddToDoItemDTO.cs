using System.ComponentModel.DataAnnotations;
using ToDoNetCore.Models;

namespace ToDoNetCore.Dtos.ToDoItem
{
    public class AddToDoItemDTO
    {
        [Required]
        public string Name { get; set; }

        public TodoItem Convert()
        {
            return new TodoItem 
            {
                Name = Name
            };
        }
    }
}