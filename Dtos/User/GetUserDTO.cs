namespace ToDoNetCore.Dtos.User
{
    public class GetUserDTO
    {
        public int Id { get; set; }
        public string Username { get; set; }
    }
}