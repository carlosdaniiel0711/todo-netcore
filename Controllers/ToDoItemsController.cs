using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using System.Linq;

using ToDoNetCore.Models;
using ToDoNetCore.Dtos.ToDoItem;
using ToDoNetCore.Services;
using ToDoNetCore.Services.ToDoService;
using Microsoft.AspNetCore.Authorization;
using System.Net.Http;

namespace ToDoNetCore.Controllers
{
    [Authorize]
    [Route("api/v1/[controller]")]
    [ApiController]
    public class ToDoItemsController : ControllerBase
    {
        private readonly IToDoService _service;

        public ToDoItemsController(IToDoService service)
        {
            _service = service;
        }

        [HttpGet("err")]
        public ActionResult<ServiceResponse<string>> GenerateErr() {
            throw new HttpRequestException("Erro gerado propositalmente!");
        }

        [HttpGet("who-is")]
        public ActionResult<ServiceResponse<string>> WhoIs()
        {
            var loginOfUser = User.Claims.FirstOrDefault(claim => claim.Type == System.Security.Claims.ClaimTypes.Name).Value;


            var response = new ServiceResponse<string>
            {
                Data = loginOfUser
            };


            return Ok(response);
        }

        // GET: api/ToDoItems
        [HttpGet]
        public async Task<ActionResult<ServiceResponse<IEnumerable<TodoItem>>>> GetToDoItems([FromServices] IToDoService _myService)
        {
            return Ok(await _myService.GetAll());
        }

        // GET: api/ToDoItems/5
        [AllowAnonymous]
        [HttpGet("{id}")]
        public async Task<ActionResult<ServiceResponse<TodoItem>>> GetTodoItem(long id)
        {
            var response = await _service.GetById(id);
            return Ok(response);
        }

        // PUT: api/ToDoItems/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutTodoItem(long id, TodoItem todoItem)
        {
            if (id != todoItem.Id)
            {
                return BadRequest();
            }
            else
            {
                await _service.Update(todoItem);
                return NoContent();
            }
        }

        // POST: api/ToDoItems
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPost]
        public async Task<ActionResult<TodoItem>> PostTodoItem(AddToDoItemDTO objDTO)
        {
            var item = await _service.Insert(objDTO.Convert());
            // return CreatedAtAction(nameof(GetTodoItem), new { id = todoItem.Id }, todoItem);
            return CreatedAtAction(nameof(GetTodoItem), new { id = item.Id }, item);
        }

        [HttpPost("collection")]
        public async Task<ActionResult<IEnumerable<TodoItem>>> PostCollection(IEnumerable<AddToDoItemDTO> itemsDTO)
        {
            return Ok(await _service.Insert(itemsDTO.Select(item => item.Convert())));
        }

        // DELETE: api/ToDoItems/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<TodoItem>> DeleteTodoItem(long id)
        {
            return Ok(await _service.Delete(id));
        }
    }
}
