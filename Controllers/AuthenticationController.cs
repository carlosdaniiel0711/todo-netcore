using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

using ToDoNetCore.Services;
using ToDoNetCore.Dtos.User;
using ToDoNetCore.Data;

namespace ToDoNetCore.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class AuthenticationController : ControllerBase
    {
        private readonly IAuthRepository _service;

        public AuthenticationController(IAuthRepository service)
        {
            _service = service;
        }

        [HttpPost("register")]
        public async Task<ActionResult<ServiceResponse<GetUserDTO>>> Register([FromBody] RegisterUserDTO objDTO)
        {
            return Ok(await _service.Register(objDTO.Username, objDTO.Password));
        }

        [HttpPost("login")]
        public async Task<ActionResult<ServiceResponse<GetUserDTO>>> Login([FromBody] RegisterUserDTO objDTO)
        {
            return Ok(await _service.Login(objDTO.Username, objDTO.Password));
        }
    }
}